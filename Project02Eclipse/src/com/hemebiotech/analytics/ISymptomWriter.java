package com.hemebiotech.analytics;

import java.util.Map;

public interface ISymptomWriter {
    void writeSymptomsToFile(Map<String ,Integer> mapSymptoms);
}
