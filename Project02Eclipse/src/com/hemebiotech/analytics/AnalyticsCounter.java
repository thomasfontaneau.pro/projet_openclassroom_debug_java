package com.hemebiotech.analytics;

import java.util.List;
import java.util.Map;

/**
 * Main class of project
 */
public class AnalyticsCounter {
    public static void main(String[] args) {
        final ISymptomReader iSymptomReader = new ReadSymptomDataFromFile("../Project02Eclipse/symptoms.txt");
        final ISymptomCounter iSymptomCounter = new SymptomCounterImpl();
        final ISymptomWriter iSymptomWriter = new SymptomWriterImpl();
        List<String> symptoms = iSymptomReader.getSymptoms();
        Map<String,Integer> mapSymptoms = iSymptomCounter.countSymptoms(symptoms);
        iSymptomWriter.writeSymptomsToFile(mapSymptoms);
    }
}
