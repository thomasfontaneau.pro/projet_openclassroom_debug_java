package com.hemebiotech.analytics;

import java.util.List;

/**
 * Interface
 */
public interface ISymptomReader {
	List<String> getSymptoms();
}
